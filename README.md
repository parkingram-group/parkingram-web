# parkingram-web

# Installation

- `npm install`
- The following environment variables must be provided:
  - `PORT`
  - `GOOGLE_API_KEY`
  - `REACT_APP_SCOKET_IO_URL` (this is the url of the main server)
  - `REACT_APP_AUTH_URL` (this is the url of the authenification server)
- `npm start`
