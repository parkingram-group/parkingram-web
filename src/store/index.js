import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createSocketIoMiddleware from 'redux-socket.io';
import io from 'socket.io-client';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';
import { SOCKET_PREFIX } from '../actions/types';

const socket = io(process.env.REACT_APP_SOCKET_IO_URL);
const socketIoMiddleware = createSocketIoMiddleware(socket, SOCKET_PREFIX);

const log = createLogger({ diff: true, collapsed: true });

const middleware = [log, thunk, socketIoMiddleware];
const enhancers = [];

const store = createStore(
  rootReducer,
  {},
  compose(
    applyMiddleware(...middleware),
    ...enhancers
  )
);
export default store;
