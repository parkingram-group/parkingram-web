import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './App.css';
import Login from './containers/Login';
import MainPage from './containers/MainPage';
import ProtectedRoute from './components/ProtectedRoute';
import AuthStatus from './components/AuthStatus';

import { logout } from './actions';

const App = props => {
  return (
    <BrowserRouter>
      <div style={{ paddingTop: '70px' }}>
        <AuthStatus token={props.token} logout={props.logout} />
        <Route path="/login" component={Login} />
        <ProtectedRoute token={props.token} path="/" exact component={MainPage} />
      </div>
    </BrowserRouter>
  );
};

const mapStateToProps = state => {
  return {
    token: state.login.token,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      logout,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
