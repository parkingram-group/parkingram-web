import React from 'react';
import { Marker } from 'react-google-maps';

const CustomMarker = props => {
  const onMarkerClick = () => {
    if (props.onCustomClick) {
      const { lat, lng } = props.position;
      props.onCustomClick({
        latitude: lat,
        longitude: lng,
      });
    }
  };

  return <Marker onClick={onMarkerClick} {...props} />;
};

export default CustomMarker;
