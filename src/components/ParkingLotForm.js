import React from 'react';
import { Alert, Form, Button } from 'react-bootstrap';

export default class ParkingLotForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      slots: '',
      missingInformation: false,
    };
  }

  onNameChange = event => {
    this.setState({
      name: event.target.value,
      missingInformation: false,
    });
  };

  onSlotsChange = event => {
    this.setState({
      slots: event.target.value,
      missingInformation: false,
    });
  };

  createParkingLot = () => {
    if (!this.state.name || !this.state.slots) {
      this.setState({ missingInformation: true });
    } else {
      const newParkingLot = {
        latitude: this.props.selectedParkingLot.latitude,
        longitude: this.props.selectedParkingLot.longitude,
        address: this.props.selectedParkingLot.address,
        name: this.state.name,
        slots: this.state.slots,
      };

      this.props.newParkingLotRequest(newParkingLot);
    }
  };

  deleteParkingLot = () => {
    this.props.deleteParkingLotRequest(this.props.selectedParkingLot);
  };

  render() {
    return (
      <div style={this.props.style}>
        <Form>
          <Form.Group controlId="formLatitude">
            <Form.Label>Latitude</Form.Label>
            <Form.Control
              readOnly
              type="number"
              value={this.props.selectedParkingLot ? this.props.selectedParkingLot.latitude : ''}
            />
          </Form.Group>
          <Form.Group controlId="formLongitude">
            <Form.Label>Longitude</Form.Label>
            <Form.Control
              readOnly
              type="number"
              value={this.props.selectedParkingLot ? this.props.selectedParkingLot.longitude : ''}
            />
          </Form.Group>
          <Form.Group controlId="formAddress">
            <Form.Label>Address</Form.Label>
            <Form.Control
              readOnly
              type="text"
              value={this.props.selectedParkingLot ? this.props.selectedParkingLot.address : ''}
            />
          </Form.Group>
          <Form.Group controlId="formName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              value={
                this.props.selectedParkingLot && this.props.selectedParkingLot.name
                  ? this.props.selectedParkingLot.name
                  : this.state.name
              }
              onChange={this.onNameChange}
            />
          </Form.Group>
          <Form.Group controlId="formSlots">
            <Form.Label>Slots</Form.Label>
            <Form.Control
              type="number"
              value={
                this.props.selectedParkingLot && this.props.selectedParkingLot.slots
                  ? this.props.selectedParkingLot.slots
                  : this.state.slots
              }
              onChange={this.onSlotsChange}
            />
          </Form.Group>
          {this.state.missingInformation ? (
            <Alert variant="danger">There are some missing information!</Alert>
          ) : null}
          {this.props.selectedParkingLot && this.props.selectedParkingLot.name ? (
            <Button onClick={this.deleteParkingLot}>Delete parking lot</Button>
          ) : (
            <Button onClick={this.createParkingLot}>Create parking lot</Button>
          )}
        </Form>
      </div>
    );
  }
}
