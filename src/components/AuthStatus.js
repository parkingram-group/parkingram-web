import React from 'react';
import { withRouter } from 'react-router-dom';
import { Navbar, Button } from 'react-bootstrap';

const AuthStatus = withRouter(({ history, token, logout }) => (
  <Navbar bg="light" expand="lg" fixed="top">
    <Navbar.Brand href="#home">Parkingram</Navbar.Brand>
    {token ? (
      <Button
        variant="outline-primary"
        onClick={() => {
          logout();
          history.push('/');
        }}
      >
        Sign out
      </Button>
    ) : null}
  </Navbar>
));

export default AuthStatus;
