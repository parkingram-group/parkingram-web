import {
  PARKING_LOT_SELECTED,
  NEW_PARKING_LOT_REQUEST,
  SEND_LOCATION_TO_SERVER_REQUEST,
  DELETE_PARKING_LOT_REQUEST,
} from './types';

export const parkingLotSelected = parkingLot => {
  return {
    type: PARKING_LOT_SELECTED,
    payload: parkingLot,
  };
};

export const newParkingLotRequest = parkingLot => {
  return {
    type: NEW_PARKING_LOT_REQUEST,
    payload: parkingLot,
  };
};

export const deleteParkingLotRequest = parkingLot => {
  return {
    type: DELETE_PARKING_LOT_REQUEST,
    payload: parkingLot,
  };
};

export const sendLocationToServer = (
  location,
  maxDistanceFromDestination,
  maxNumberOfParkingLots
) => {
  const message = {
    maxDistanceFromDestination,
    destination: location,
    currentLocation: location,
    maxNumberOfParkingLots,
  };
  return {
    type: SEND_LOCATION_TO_SERVER_REQUEST,
    payload: message,
  };
};
