import { LOGIN_REQUEST, LOGIN_SOCKET_IO_REQUEST, LOGIN_ERROR, LOGOUT_REQUEST } from './types';

export const loginRequest = (username, password) => async dispatch => {
  dispatch({
    type: LOGIN_REQUEST,
    payload: null,
  });

  try {
    const response = await fetch(`${process.env.REACT_APP_AUTH_URL}/login`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    });

    if (!response.ok) {
      throw Error(`Request rejected with status ${response.status}`);
    }

    const json = await response.json();

    dispatch({
      type: LOGIN_SOCKET_IO_REQUEST,
      payload: json.access_token,
    });
  } catch (error) {
    dispatch({
      type: LOGIN_ERROR,
      payload: error,
    });
  }
};

export const logout = () => {
  return {
    type: LOGOUT_REQUEST,
    payload: {},
  };
};
