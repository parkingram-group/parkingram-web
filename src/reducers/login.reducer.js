import { LOGIN_REQUEST, LOGIN_ERROR, LOGIN_SUCCESS, LOGOUT_SUCCESS } from '../actions/types';

const INITIAL_STATE = {
  isLoading: false,
  token: '',
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return { ...state, isLoading: true };
    case LOGIN_ERROR:
      return { ...state, isLoading: false, token: '' };
    case LOGIN_SUCCESS:
      return { ...state, isLoading: false, token: action.payload };
    case LOGOUT_SUCCESS:
      return { ...state, token: '' };
    default:
      return state;
  }
}
