import { combineReducers } from 'redux';
import login from './login.reducer';
import parkingLots from './parkingLots.reducers';

const rootReducer = combineReducers({
  login,
  parkingLots,
});

export default rootReducer;
