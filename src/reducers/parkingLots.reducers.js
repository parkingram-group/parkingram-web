import {
  GET_PARKING_LOTS_SUCCESS,
  PARKING_LOT_SELECTED,
  SEND_LOCATION_TO_SERVER_SUCCESS,
  NEW_PARKING_LOT_ERROR,
  NEW_PARKING_LOT_SUCCESS,
  DELETE_PARKING_LOT_ERROR,
  DELETE_PARKING_LOT_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
  existingParkingLots: [],
  selectedParkingLot: {
    latitude: 46.756137,
    longitude: 23.549879,
  },
  success: false,
  error: false,
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_PARKING_LOTS_SUCCESS:
      return { ...state, existingParkingLots: action.payload };
    case PARKING_LOT_SELECTED:
      return { ...state, selectedParkingLot: action.payload };
    case SEND_LOCATION_TO_SERVER_SUCCESS:
      return { ...state, existingParkingLots: action.payload };
    case NEW_PARKING_LOT_ERROR:
      return {
        ...state,
        selectedParkingLot: INITIAL_STATE.selectedParkingLot,
        success: false,
        error: true,
      };
    case NEW_PARKING_LOT_SUCCESS:
      return {
        ...state,
        selectedParkingLot: INITIAL_STATE.selectedParkingLot,
        success: true,
        error: false,
      };
    case DELETE_PARKING_LOT_ERROR:
      return {
        ...state,
        selectedParkingLot: INITIAL_STATE.selectedParkingLot,
        success: false,
        error: true,
      };
    case DELETE_PARKING_LOT_SUCCESS:
      return {
        ...state,
        selectedParkingLot: INITIAL_STATE.selectedParkingLot,
        success: true,
        error: false,
      };
    default:
      return state;
  }
}
