import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { Alert, Form, Button } from 'react-bootstrap';
import { loginRequest } from '../actions';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      wasLoginTrial: false,
    };
  }

  onUsernameChange = event => {
    this.setState({
      username: event.target.value,
      wasLoginTrial: false,
    });
  };

  onPasswordChange = event => {
    this.setState({
      password: event.target.value,
      wasLoginTrial: false,
    });
  };

  onSubmit = event => {
    event.preventDefault();
    this.setState({ wasLoginTrial: true });
    this.props.loginRequest(this.state.username, this.state.password);
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };

    if (this.props.token) {
      return <Redirect to={from} />;
    }

    return (
      <div style={{ padding: 10 }}>
        <Form onSubmit={this.onSubmit}>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control
              id="username"
              type="text"
              value={this.state.username}
              onChange={this.onUsernameChange}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              id="password"
              type="password"
              value={this.state.password}
              onChange={this.onPasswordChange}
            />
          </Form.Group>
          {this.state.wasLoginTrial ? (
            <Alert variant="danger">The given credentials are incorrect!</Alert>
          ) : null}
          <Button type="submit">Submit</Button>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.login.token,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      loginRequest,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
