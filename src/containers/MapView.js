import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import { Button } from 'react-bootstrap';
import CustomMarker from '../components/CustomMarker';

const GoogleMapsWrapper = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap {...props} ref={props.onMapMounted}>
      {props.children}
    </GoogleMap>
  ))
);

export default class MapView extends React.Component {
  render() {
    return (
      <div style={this.props.style}>
        <GoogleMapsWrapper
          ref={c => {
            this.map = c;
          }}
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDk8x6ebbUQUbJR7i2jYAB3UFm0hN8XWjA&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: '100%' }} />}
          containerElement={<div style={{ height: '500px' }} />}
          mapElement={<div style={{ height: '100%' }} />}
          defaultZoom={12}
          center={{
            lat: this.props.selectedParkingLot.latitude,
            lng: this.props.selectedParkingLot.longitude,
          }}
        >
          {this.props.existingParkingLots.map(marker => (
            <CustomMarker
              key={`${marker.latitude}-${marker.longitude}`}
              position={{ lat: marker.latitude, lng: marker.longitude }}
              onCustomClick={this.props.parkingLotSelected}
            />
          ))}
          {this.props.selectedParkingLot && (
            <CustomMarker
              key={`${this.props.selectedParkingLot.latitude}-${this.props.selectedParkingLot.longitude}`}
              position={{
                lat: this.props.selectedParkingLot.latitude,
                lng: this.props.selectedParkingLot.longitude,
              }}
              onCustomClick={this.props.parkingLotSelected}
            />
          )}
        </GoogleMapsWrapper>
        <Button
          onClick={() => {
            this.props.sendLocationToServer(this.props.selectedParkingLot, 20, 20);
          }}
        >
          Search registered parking lots near the selected area
        </Button>
      </div>
    );
  }
}
