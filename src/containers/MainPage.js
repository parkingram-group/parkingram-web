import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Alert } from 'react-bootstrap';
import MapView from './MapView';
import SearchBox from '../components/SearchBox';

import {
  parkingLotSelected,
  newParkingLotRequest,
  deleteParkingLotRequest,
  sendLocationToServer,
} from '../actions';
import ParkingLotForm from '../components/ParkingLotForm';

const MainPage = props => (
  <div style={{ textAlign: 'center' }}>
    <SearchBox onSelect={props.parkingLotSelected} style={{ padding: 10 }} />
    <MapView
      existingParkingLots={props.existingParkingLots}
      selectedParkingLot={props.selectedParkingLot}
      parkingLotSelected={props.parkingLotSelected}
      style={{ padding: 10 }}
      sendLocationToServer={props.sendLocationToServer}
    />
    {props.success ? (
      <div>
        <Alert variant="success">Operation successful!</Alert>
      </div>
    ) : null}
    {props.error ? (
      <div>
        <Alert variant="danger">Operation unsuccessful!</Alert>
      </div>
    ) : null}
    <ParkingLotForm
      selectedParkingLot={
        props.existingParkingLots.find(
          parkingLot =>
            parkingLot.latitude === props.selectedParkingLot.latitude &&
            parkingLot.longitude === props.selectedParkingLot.longitude
        ) || props.selectedParkingLot
      }
      newParkingLotRequest={props.newParkingLotRequest}
      deleteParkingLotRequest={props.deleteParkingLotRequest}
      style={{ padding: 10 }}
    />
  </div>
);

const mapStateToProps = state => {
  return {
    existingParkingLots: state.parkingLots.existingParkingLots,
    selectedParkingLot: state.parkingLots.selectedParkingLot,
    success: state.parkingLots.success,
    error: state.parkingLots.error,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      parkingLotSelected,
      newParkingLotRequest,
      deleteParkingLotRequest,
      sendLocationToServer,
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);
